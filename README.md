# Marvel

Application available on https://marvel-webapp-2nsbevs3oq-ew.a.run.app

## Technical stacks

- [Vue 3](https://v3.vuejs.org/)
- [Vuetify](https://next.vuetifyjs.com/)
- [Express](https://expressjs.com/)
- [Cloud build](https://cloud.google.com/build)
- [Cloud run](https://cloud.google.com/run)
- [Secret Manager](https://cloud.google.com/secret-manager)

## Preconditon

- Create your developer account on https://developer.marvel.com
- Node v16 (install [n](https://www.npmjs.com/package/n?activeTab=versions) for node version manager)
- yarn
- direnv (optional): https://direnv.net/
  Facilitates the use of environment variables

## Setup environment variables

`export MARVEL_PUBLIC_KEY=<Your marvel public key>`

`export MARVEL_PRIVATE_KEY=<Your marvel private key>`

## How to run the app with docker locally

- Build images

  `yarn docker:build-local`

- Run Docker Compose

  `yarn docker:up`

## Development Mode

- Install dependencies

  `yarn install`

- Start backend dev server from root repository

  `cd ./backend`

  `yarn start:dev`

- Start webapp dev server from root repository

  `cd ./webapp`

  `yarn start:dev`

## Improvment

- Deploy Secret from CICD
- Character description feature
- Test e2e
- Complete TUs
