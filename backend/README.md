# backend

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn start:dev
```

### Run your unit tests

```
yarn test
```

### Lints and fixes files

```
yarn lint
```
