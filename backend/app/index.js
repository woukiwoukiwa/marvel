import express from 'express';
import cors from 'cors';
import characters from './modules/characters/characters.controller';

const app = express();
const port = process.env.PORT;

app.use(cors());

app.use('/api/characters', characters);

app.listen(port, () => {
    console.log(`Marvel API listening at http://localhost:${port}`)
})