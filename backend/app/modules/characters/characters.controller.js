import express from 'express';
import characterService from './characters.service';
import { StatusCodes } from 'http-status-codes';

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const result = await characterService.findCharacters(req.query);
        res.send(result)
    } catch (error) {
        if (error.response)
            res.status(error.response.status).send(error.response.statusText);
        res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error.message);
    }
});

export default router;