import axios from 'axios';
import md5 from 'md5';

const marvelApiPublicKey = process.env.MARVEL_PUBLIC_KEY;
const marvelApiPrivateKey = process.env.MARVEL_PRIVATE_KEY;

const buildAuth = () => {
    const ts = new Date().getTime();
    const hash = md5(`${ts}${marvelApiPrivateKey}${marvelApiPublicKey}`);
    return {
        apikey: marvelApiPublicKey,
        hash,
        ts,
    }
};

export default {
    findCharacters: async ({ limit, offset, nameStartsWith }) => {
        const response = await axios.get('http://gateway.marvel.com/v1/public/characters', {
            params: {
                ...buildAuth(),
                limit,
                offset,
                nameStartsWith,
            }
        });
        const data = response.data;
        const resultDetail = {
            offset: data.data.offset,
            limit: data.data.limit,
            total: data.data.total,
            count: data.data.count,
        };
        const characters = data.data.results.map(character => {
            return {
                id: character.id,
                name: character.name,
                description: character.description,
                thumbnail: {
                    standart: `${character.thumbnail.path}/standard_fantastic.${character.thumbnail.extension}`,
                    fullSize: `${character.thumbnail.path}.${character.thumbnail.extension}`,
                }
            };
        });
        return {
            copyright: data.copyright,
            attributionText: data.attributionText,
            attributionHTML: data.attributionHTML,
            resultDetail,
            characters
        };
    }
}