import axios from 'axios';
import characterService from './characters.service';
import {jest, test, expect} from '@jest/globals'

jest.mock('axios');

const marvelApiResult = {
    "data": {
        "code": 200,
        "status": "Ok",
        "copyright": "© 2021 MARVEL",
        "attributionText": "Data provided by Marvel. © 2021 MARVEL",
        "attributionHTML": "<a href=\"http://marvel.com\">Data provided by Marvel. © 2021 MARVEL</a>",
        "etag": "001",
        "data": {
            "offset": 0,
            "limit": 1,
            "total": 1504,
            "count": 1,
            "results": [
                {
                    "id": "001",
                    "name": "Wouki Man",
                    "description": "Best MCU character ever",
                    "modified": "2014-04-29T14:18:17-0400",
                    "thumbnail": {
                        "path": "http://thumbnail",
                        "extension": "jpg"
                    },
                    "resourceURI": "http://gateway.marvel.com/v1/public/characters/001",
                    "comics": {
                        "available": 1,
                        "collectionURI": "http://gateway.marvel.com/v1/public/characters/001/comics",
                        "items": [
                            {
                                "resourceURI": "http://gateway.marvel.com/v1/public/comics/001",
                                "name": "Avengers: The Initiative (2007) #14"
                            }
                        ],
                        "returned": 1
                    },
                    "series": {
                        "available": 1,
                        "collectionURI": "http://gateway.marvel.com/v1/public/characters/001/series",
                        "items": [
                            {
                                "resourceURI": "http://gateway.marvel.com/v1/public/series/001",
                                "name": "Marvel Premiere (1972 - 1981)"
                            }
                        ],
                        "returned": 1
                    },
                    "stories": {
                        "available": 1,
                        "collectionURI": "http://gateway.marvel.com/v1/public/characters/001/stories",
                        "items": [
                            {
                                "resourceURI": "http://gateway.marvel.com/v1/public/stories/001",
                                "name": "Avengers: The Initiative (2007) #14, Spotlight Variant - Int",
                                "type": "interiorStory"
                            }
                        ],
                        "returned": 1
                    },
                    "events": {
                        "available": 1,
                        "collectionURI": "http://gateway.marvel.com/v1/public/characters/001/events",
                        "items": [
                            {
                                "resourceURI": "http://gateway.marvel.com/v1/public/events/001",
                                "name": "Secret Invasion"
                            }
                        ],
                        "returned": 1
                    },
                    "urls": [
                        {
                            "type": "detail",
                            "url": "http://marvel.com/characters/001"
                        }
                    ]
                }
            ]
        }
    }
}

test('get marvel characters', async () => {
    const result = {
        copyright: "© 2021 MARVEL",
        attributionText: "Data provided by Marvel. © 2021 MARVEL",
        attributionHTML: "<a href=\"http://marvel.com\">Data provided by Marvel. © 2021 MARVEL</a>",
        resultDetail: {
            offset: 0,
            limit: 1,
            total: 1504,
            count: 1,
        },
        characters: [{
            id: "001",
            name: "Wouki Man",
            description: "Best MCU character ever",
            thumbnail: {
                standart: "http://thumbnail/standard_fantastic.jpg",
                fullSize: "http://thumbnail.jpg",
            }
        }]
    };
    const resp = marvelApiResult;
    axios.get.mockResolvedValue(resp);


    const response = await characterService.findCharacters({});
    expect(axios.get).toHaveBeenCalledTimes(1);
    expect(response).toEqual(result);
});