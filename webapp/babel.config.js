module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset'
  ],
  plugins: [
        [
            'module-resolver',
            {
                extensions: ['.js', '.jsx', '.ts', '.tsx', '.png', '.vue'],
                alias: {
                    '@': './src',
                    '@test': './tests',
                    '@characters': './src/features/characters',
                },
            },
        ],
    ],
}
