import { shallowMount } from "@vue/test-utils";
import MarvelAttribution from "@characters/components/molecules/MarvelAttribution.vue";

describe("MarvelAttribution.vue", () => {
  it("renders props.msg when passed", () => {
    const attributionText = "Attribution Text";
    const wrapper = shallowMount(MarvelAttribution, {
      props: {
        attributionText,
      },
    });
    expect(wrapper.text()).toMatch(attributionText);
  });
});
