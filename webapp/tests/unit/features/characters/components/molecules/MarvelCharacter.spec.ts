import { shallowMount } from "@vue/test-utils";
import MarvelCharacter from "@characters/components/molecules/MarvelCharacter.vue";
import { Character } from "@characters/services/types";

describe("MarvelCharacter.vue", () => {
  it("renders props.msg when passed", () => {
    const character: Character = {
      id: "0001",
      name: "Wouki Man",
      description: "",
      thumbnail: {
        fullSize: "",
        standart: "",
      },
    };
    const wrapper = shallowMount(MarvelCharacter, {
      props: {
        character,
      },
    });
    expect(wrapper.text()).toMatch("Wouki Man");
  });
});
