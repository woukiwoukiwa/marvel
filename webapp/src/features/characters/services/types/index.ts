export const CHARACTERS_ENDPOINT = "characters";

export interface FindCharactersRequest {
  offset?: number;
  limit?: number;
  nameStartsWith?: string;
}

export interface FindCharactersResponse {
  copyright: string;
  attributionText: string;
  attributionHTML: string;
  resultDetail: FindResultDetail;
  characters: Character[];
}

export interface FindResultDetail {
  offset: number;
  limit: number;
  total: number;
  count: number;
}

export interface Character {
  id: string;
  name: string;
  description: string;
  thumbnail: {
    standart: string;
    fullSize: string;
  };
}
