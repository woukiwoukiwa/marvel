import axios from "axios";
import {
  CHARACTERS_ENDPOINT,
  FindCharactersRequest,
  FindCharactersResponse,
} from "./types";

const API_ENDPOINT = `${process.env.VUE_APP_BACKEND_API}/${CHARACTERS_ENDPOINT}`;

export const findCharacters = async (criteria?: {
  offset?: number;
  limit?: number;
  nameStartsWith?: string;
}): Promise<FindCharactersResponse> => {
  const request: FindCharactersRequest = {};
  if (criteria) {
    request.limit = criteria.limit;
    request.offset = criteria.offset;
    request.nameStartsWith = criteria.nameStartsWith || undefined;
  }
  const response = await axios.get(API_ENDPOINT, {
    params: request,
  });
  return response.data;
};
