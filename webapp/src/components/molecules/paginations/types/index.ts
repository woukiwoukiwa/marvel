export interface PaginationConfig {
  offset: number;
  limit: number;
  total: number;
}

export interface PaginationEvent {
  offset: number;
  limit: number;
}
