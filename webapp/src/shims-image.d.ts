declare module "*.png" {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const png: any;
  export default png;
}
declare module "*.jpeg" {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const jpeg: any;
  export default jpeg;
}
