import { computed, ComputedRef, WritableComputedRef } from "vue";

export function useModelWrapper<T>(
  props: Readonly<Record<string, unknown>>,
  emit: (event: "update:modelValue", ...args: unknown[]) => void
): ComputedRef<T> {
  return computed<T>({
    get: () => props.modelValue as T,
    set: (value) => emit("update:modelValue", value),
  });
}

export function useModelWrapperArg<T>(
  props: Readonly<Record<string, unknown>>,
  emit: (event: string, ...args: unknown[]) => void,
  name = "modelValue"
): WritableComputedRef<T> {
  return computed({
    get: () => props[name] as T,
    set: (value) => emit(`update:${name}`, value),
  });
}
