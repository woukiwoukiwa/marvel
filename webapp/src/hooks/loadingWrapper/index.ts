import { Ref, ref } from "vue";

export function useLoadingWrapper<T>(
  fn: (...args: unknown[]) => Promise<T>,
  getKey?: (...args: unknown[]) => string
): [
  Ref<boolean>,
  Ref<string>,
  (...args: unknown[]) => Promise<T>,
  Ref<string>
] {
  const loading = ref(false);
  const error = ref();
  const key = ref("");
  const callFunc = async (...args: unknown[]): Promise<T> => {
    error.value = undefined;
    let result: T | undefined = undefined;
    if (getKey) key.value = getKey(...args);
    try {
      loading.value = true;
      result = await fn(...args);
    } catch (e) {
      error.value = e.message;
      throw e;
    } finally {
      loading.value = false;
    }
    return result as T;
  };
  return [loading, error, callFunc, key];
}
